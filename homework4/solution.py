from copy import deepcopy
from typing import Iterable


def ilen(iterable: Iterable):
    """Функция для получения размера генератора"""
    counter = 0
    for el in iterable:
        counter += 1
    return counter


def flatten(iterable: Iterable):
    """Функция из многоуровневого массива сделает одноуровневый"""
    new_list = []
    for el in iterable:
        # Check if it is a value or another Iterable. String is also Iterable
        if not isinstance(el, Iterable) or isinstance(el, str):
            new_list.append(el)
        else:
            buff_list = flatten(el)
            new_list += buff_list
    return new_list


def distinct(iterable: Iterable):
    """Функция удаляет дубликаты, сохраняя порядок"""
    final_list = []
    for elem in iterable:
        if elem not in final_list:
            final_list.append(elem)
    return final_list


def groupby(key, iterable: Iterable):
    """Функция возвращает неупорядоченную послежовательность из словарей, сгрупированных по ключу"""
    new_dict = dict()
    for el in iterable:
        if isinstance(el, dict):
            el_key = el[key]
            if el_key not in new_dict:
                new_dict[el_key] = []
                new_dict[el_key].append(el)
            else:
                new_dict[el_key].append(el)
    return new_dict


def chunks(size: int, iterable: Iterable):
    """Функция разбивает последовательность на заданные куски"""
    buff_list = list()
    final_list = list()
    counter = 0
    for i in iterable:
        buff_list.append(i)
        counter += 1
        if counter == size:
            buff_list_2 = deepcopy(buff_list)
            final_list.append(buff_list_2)
            buff_list.clear()
            counter = 0
    if counter != 0:
        final_list.append(buff_list)
    return final_list


def first(iterable: Iterable):
    """Функция возвращает первый элемент или None"""
    if isinstance(iterable, set):
        raise TypeError('Нет очередности!')
    elif isinstance(iterable, dict):
        for el in iterable.items():
            return {el[0]: el[1]}
    else:
        for el in iterable:
            return el


def last(iterable: Iterable):
    """Функция возвращает последний элемент или None"""
    if isinstance(iterable, set):
        raise TypeError('Нет очередности!')
    elif isinstance(iterable, dict):
        el = None
        for el in iterable.items():
            pass
        return {el[0]: el[1]}
    else:
        el = None
        for el in iterable:
            pass
        return el
