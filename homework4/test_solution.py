# projectroot/tests/test_code.py
from homework4 import solution as s


def test_ilen_int():
    assert s.ilen([0 for j in range(0, 10)]) == 10, "Should be 10"


def test_ilen_string():
    assert s.ilen(["A", "B", "C", "D"]) == 4, "Should be 4"


def test_ilen_iterable():
    assert s.ilen([["A", "B", "C"], [1, 2, ["D", "E"]], "B", "C", "D"]) == 5, "Should be 5"


def test_flattern():
    assert len(s.flatten([["A", "B", "C"], [1, 2, ["D", "E"]], "B", "C", "D"])) == 10, "Should be 10"


def test_flattern_value():
    must_return_value = ["A", "B", "C", 1, 2, "D", "E", "B", "C", "D"]
    value_after_func_exec = s.flatten([["A", "B", "C"], [1, 2, ["D", "E"]], "B", "C", "D"])
    assert value_after_func_exec == must_return_value, str(value_after_func_exec) + " Should be " + str(must_return_value)


def test_distinct():
    assert len(s.distinct(["A", "B", "C", 1, ["A", "B"], "A", ["A", "B"]])) == 5, "Should be 5"


def test_distinct_value():
    must_return_value = ["A", "B", "C", 1, ["A", "B"]]
    value_after_func_exec = s.distinct(["A", "B", "C", 1, ["A", "B"], "A", ["A", "B"]])
    must_return_value == value_after_func_exec, str(value_after_func_exec) + " Should be " + str(must_return_value)


def test_group_by():
    users = [
        {'gender': 'female', 'age': 33},
        {'gender': 'male', 'age': 20},
        {'gender': 'female', 'age': 21},
    ]
    assert len(s.groupby('gender', users)) == 2, "Should be 2"


def test_group_by_value():
    users = [
        {'gender': 'female', 'age': 33},
        {'gender': 'male', 'age': 20},
        {'gender': 'female', 'age': 21},
    ]
    must_return_value = {
        'female': [
            {'gender': 'female', 'age': 33},
            {'gender': 'female', 'age': 21},
        ],
        'male': [{'gender': 'male', 'age': 20}],
    }
    value_after_func_exec = s.groupby('gender', users)
    assert value_after_func_exec == must_return_value, str(value_after_func_exec) + " Should be " + str(must_return_value)


def test_chunks():
    assert len(s.chunks(3, [0, 1, 2, 3, 4])) == 2, "Should be 2"


def test_chunks_value():
    must_return_value = [[0, 1, 2], [3, 4, ]]
    value_after_func_exec = s.chunks(3, [0, 1, 2, 3, 4])
    assert value_after_func_exec == must_return_value, str(value_after_func_exec) + " Should be " + str(must_return_value)


def test_first_value():
    must_return_value = 0
    value_after_func_exec = s.first([0, 1, 532, 21, 29])
    assert value_after_func_exec == must_return_value, str(value_after_func_exec) + " Should be " + str(must_return_value)


def test_last_value():
    must_return_value = 29
    value_after_func_exec = s.last([0, 1, 532, 21, 29])
    assert value_after_func_exec == must_return_value, str(value_after_func_exec) + " Should be " + str(must_return_value)
