
class Color(object):
    _cached = {}

    def __new__(cls, red, green, blue):
        try:
            return cls._cached[red, green, blue]
        except KeyError:
            x = super(Color, cls).__new__(cls)
            x.__init__(red, green, blue)
            cls._cached[red, green, blue] = x
            return x

    # Color initializing
    def __init__(self, red, green, blue):
        self.red_level = red
        self.green_level = green
        self.blue_level = blue

    # Color as string
    def __str__(self):
        END = '\033[0'
        START = '\033[1;38;2'
        MOD = 'm'
        return f'{START};{self.red_level};{self.green_level};{self.blue_level}{MOD}•{END}{MOD}'

    def __repr__(self):
        return self.__str__()

    # equality ( color1 == color2 )
    def __eq__(self, other):
        return self.red_level == other.red_level and self.green_level == other.green_level and self.blue_level == other.blue_level

    # sum of colors
    def __add__(self, other):
        self.red_level = self.red_level + other.red_level
        self.green_level = self.green_level + other.green_level
        self.blue_level = self.blue_level + other.blue_level

    # contrast ratio change
    def __mul__(self, value):
        try:
            assert 0 <= value <= 1
            redlevel = self.red_level
            greenlevel = self.green_level
            bluelevel = self.blue_level
            cl = -256 * (1 - value)
            F = (259 * (cl + 255)) / (255 * (259 - cl))
            self.red_level = round(F * (redlevel - 128) + 128)
            self.green_level = round(F * (greenlevel - 128) + 128)
            self.blue_level = round(F * (bluelevel - 128) + 128)
            return self
        except AssertionError:
            print("Value should be greater or equal than 0 and less or equal than 1")

red = Color(255, 0, 0)
green = Color(0, 255, 0)
print(repr(red))
print("Red dot : " + str(red))
print("Green dot: " + str(green))
print("Red equal to green: " + str(red == green))
print("Red equal to red: " + str(red == red))
print("Mixing of colors: red + green: " + str(red + green))
print("Change of contrast level, green color: " + str(green * 0.5))

orange1 = Color(255, 165, 0)
orange2 = Color(255, 165, 0)

print("orange1 is orange2?: " + str(orange1 is orange2))
print("red is green?: " + str(red is green))
