dictionary = ["a", "b", "c", "d", "e"]


def create_play_field() -> list:
    """Request playing field size and construct it"""
    fieldsize = input("Please enter field size: ")
    try:
        assert 3 <= int(fieldsize) <= 5
        field = [[0 for j in range(0, int(fieldsize))] for i in range(0, int(fieldsize))]
        for i in range(int(fieldsize)):
            for j in range(int(fieldsize)):
                field[i][j] = "E"
        return field
    except AssertionError:
        raise AssertionError("Range of playing field should be greater or equal to 3 and less or equal to 5")


def print_field():
    """Print field"""
    print("\n  ", end="")
    for i in range(len(field)):
        print(str(i + 1) + "   ", end="")
    for i in range(len(field)):
        print("\n" + dictionary[i], end="")
        for j in range(len(field)):
            print(" " + field[i][j] + "  ", end="")


def check_for_win(symbol: str) -> bool:
    # Check rows
    result = False
    for i in range(len(field)):
        if result:
            return result
        result = True
        for j in range(len(field)):
            if field[i][j] != symbol:
                result = False
    # Check columns
    for i in range(len(field)):
        if result:
            return result
        result = True
        for j in range(len(field)):
            if field[j][i] != symbol:
                result = False
    # Check diagonals
    result = True
    for i in range(len(field)):
        if field[i][len(field) - 1 - i] != symbol:
            result = False
    if result:
        return result

    result = True
    for i in range(len(field)):
        if field[i][i] != symbol:
            result = False
    return result


def check_for_draw() -> bool:
    for i in range(len(field)):
        for j in range(len(field)):
            if field[i][j] == "E":
                return False
    return True


def player_action(coordinate, symbol) -> bool:
    result = True
    try:
        rowletter = coordinate[0]
        column = int(coordinate[1])
        row = dictionary.index(rowletter)
        if row > len(field) or column > len(field):
            print("Coordinates must be less than matrix size!")
            result = False
        elif (field[row][column - 1]) == "x" or (field[row][column - 1]) == "0":
            print("This cell is already filled!")
            result = False
        else:
            field[row][column - 1] = symbol
        return result
    except ValueError:
        print("Incorrect input!")
        return False


field = create_play_field()
print_field()

playersymbol = "x"
print("\nGame has started!")
playernumber = 1
while True:
    correctinput = False
    while not correctinput:
        cordinate = input("\nPlayer " + str(playernumber) + " turn: ")
        correctinput = player_action(cordinate, playersymbol)
        print_field()
    if check_for_win(playersymbol):
        print("\nPlayer " + str(playernumber) + " won! Congratulations!")
        break
    if check_for_draw():
        print("\nThe game ended in a draw!")
        break

    if playernumber == 1:
        playersymbol = "0"
        playernumber = 2
    else:
        playersymbol = "x"
        playernumber = 1
