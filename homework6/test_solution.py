from homework6.solution import CharField
from homework6.solution import IntegerField
from homework6.solution import Model
from homework6.solution import SqliteDatabase

db = SqliteDatabase(":memory:")


class BaseModel(Model):
    class Meta:
        database = db


class Advert(BaseModel):
    title = CharField(max_length=128)
    price = IntegerField(min_value=0)

    def __str__(self):
        return f"{self.title} {self.price}"


if __name__ == "__main__":  # pragma: no cover
    db.connect()
    db.create_tables([Advert])

    Advert.create(title="\"iPhone_X\"", price=100)

    adverts = Advert.select()
    assert adverts[0].title == "iPhone_X"
    assert adverts[0].price == 100
