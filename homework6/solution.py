import sqlite3
from typing import List
from typing import Type

from orm import Model


class Field:

    _field_db_type: str = None

    def __get__(self, instance, owner):
        return instance.__dict__[self.name]

    def __set__(self, instance, value):
        instance.__dict__[self.name] = value

    def __set_name__(self, owner, name):
        self.name = name

    def __str__(self):
        return f"{self.name} {self._field_db_type}"

    def __repr__(self):
        return self.__str__()

    def prepare_to_insert(self, instance) -> str:
        return str(instance.__dict__[self.name])

    @property
    def field_db_type(self):
        return self._field_db_type


class CharField(Field):

    _field_db_type = "varchar"

    def __init__(self, max_length: int):
        if not isinstance(max_length, int):
            raise TypeError("max_length must be int")
        self.max_length = max_length

    def __set__(self, instance, value):
        if not isinstance(value, str):
            raise ValueError(f"{self.name} value must be str")

        if len(value) > self.max_length:
            raise ValueError(
                f"length {self.name} must be less or equal {self.max_length}"
            )

        instance.__dict__[self.name] = value


class IntegerField(Field):
    _field_db_type = "int"

    def __init__(self, min_value: int = None, max_value: int = None):
        if not isinstance(min_value, (int, type(None))):
            raise TypeError("max_value must be int")

        if not isinstance(max_value, (int, type(None))):
            raise TypeError("max_value must be int")

        self.min_value = min_value
        self.max_value = max_value

    def __set__(self, instance, value):
        if not isinstance(value, int):
            raise ValueError(f"{self.name} value must be int")

        if self.min_value is not None and self.min_value > value:
            raise ValueError(f"{self.name} value must be more than {self.min_value}")

        if self.max_value is not None and value > self.max_value:
            raise ValueError(f"{self.name} value must be less than {self.max_value}")

        instance.__dict__[self.name] = value


class Model:
    def __init__(self, **fields):
        field_names = [mf.name for mf in self.fields()]

        for field, value in fields.items():
            if field not in field_names:
                raise AttributeError(f"unknown field {field}")
            setattr(self, field, value)

    @classmethod
    def create(cls, **fields) -> "Model":
        conn = cls.Meta.database.conn
        model_name = cls.__name__
        model = cls(**fields)

        field_values = []
        model_fields = model.fields()

        for model_field in model_fields:
            field_values.append(model_field.prepare_to_insert(model))

        sql_values = ",".join(field_values)
        print(sql_values)
        sql_query = f"""
            INSERT INTO {model_name}
                 VALUES({sql_values})
        """
        print(sql_query)
        conn.execute(sql_query)
        conn.commit()

        return model

    @classmethod
    def select(cls):
        conn = cls.Meta.database.conn
        model_name = cls.__name__

        sql_query = f"""
            SELECT * FROM {model_name}
        """
        cur = conn.cursor()

        cur.execute(sql_query)
        rows = cur.fetchall()
        result = []
        names = [description[0] for description in cur.description]
        for row in rows:
            model_obj = cls()
            for name, value in zip(names, row):
                model_obj.__setattr__(name, value)
            result.append(model_obj)
        return result

    @classmethod
    def fields(cls) -> List[Field]:
        return [
            cls.__dict__[item]
            for item in cls.__dict__
            if isinstance(cls.__dict__[item], Field)
        ]


class SqliteDatabase:
    def __init__(self, database: str) -> None:
        if database == ":memory:":
            self.database = database

    def connect(self):
        self.conn = sqlite3.connect(self.database)

    def create_table(self, model: Type[Model]):
        model_name = model.__name__

        declare_fields = []
        for field in model.fields():
            declare_fields.append(f" {field.name} {field.field_db_type} ")

        sql_declare_fields = ",".join(declare_fields)
        sql_query = f"""
                CREATE TABLE {model_name}
                ({sql_declare_fields})
        """

        self.conn.execute(sql_query)

    def create_tables(self, models: List[Type[Model]]) -> None:
        for model in models:
            self.create_table(model)
