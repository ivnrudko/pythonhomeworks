initialstory = '\nВсе началось когда уточка прилетел исследовать планету IPO_31_440.' \
               '\nСогласно заданию из исследовательского центра необходимо собрать образцы почвы и' \
               '\nнекотырых редких растений. ' \
               '\nНа этом можно было хорошенько подзаработать, но путь был не близкий и планету окружал космический ' \
               '\nмусор,' \
               '\nпройти который без повреждений для корабля было очень сложно, но уточка был храбрым, но главное ' \
               '\nопытным пилотом' \
               '\nи никогда не сдавался. Космический мусор остался позади, уточка практически выполнил задание, ' \
               '\nно тут прилетели флотаны.' \
               '\nНеизвестно что им понадобилось на этой планете, но эта расса всегда славилась своей агресивностью' \
               '\nи унчитожением представителей других рас. Флотаны заметили уточку и пустились за ним в погоню.'

birdstoryend = '\nУточка поскорее постарался включить все системы корабля и улететь с этой проклятой планеты.' \
               '\nКорабль стремительно набирал высоту, в этот раз уточка смог оторваться от преследователей.'

nobirdstoryend = '\nУточка неторопясь шел к кораблю по пути, собирая недостающие образцы почвы.' \
                 '\nТоропиться было некуда. Перед тем как подняться на корабль, уточка в последний раз ' \
                 '\nвзглянул на пейзаж, после этого включил все системы корабля и начал стремительно' \
                 '\nудаляться от планеты'


def initial_step():
    print(
        '\nПришелец-утка убегал от преследователей.'
        '\nНеобходимо в кратчайшие сроки добраться до корабля и оторваться от преследователей.'
        '\nПришелец-уточка видит перед собой лес и поле.'
        '\nПобежать через лес или через поле?'
    )
    option = ""
    options = {'Побежать через лес': True, 'Побежать через поле': False}
    while option not in options:
        print('Выберите: {}/{}'.format(*options))
        option = input()

    if options[option]:
        step_2_run_through_the_forest()
    else:
        step_2_run_through_the_field()


def step_2_run_through_the_forest():
    print(
        '\nПришелец-уточка бросился в лес.'
        '\nОн надеелся таким образом запутать преследователей. Все-таки пара флотанов на хвосте это не шутка.'
        '' + initialstory + '\nСейчас необходимо в кратчайшие сроки добраться до корабля минуя этот лес. '
                            '\nУточка ориентировался по радару, как вдруг увидел инопланетную птицу. Тут совершенно '
                            '\nиррацианально уточка понял,'
                            '\nчто эта птица поможет. Но вдруг это ловушка? В центре рассказывали об инопланетных '
                            '\nформах жизни, которые '
                            '\nмогут влиять на ментальное состояние других форм жизни'
                            '\nПодойти к птице или продолжить бежать дальше?'
    )
    option = ""
    options = {'Подойти к птице': True, 'Бежать дальше': False}
    while option not in options:
        print('Выберите: {}/{}'.format(*options))
        option = input()

    if options[option]:
        step_3_go_to_the_bird()
    else:
        step_3_run_on()


def step_2_run_through_the_field():
    print(
        '\nПришелец-уточка решил бежать через поле. Он подумал, что это кратчайший путь до корабля.'
        '\nОн надеялся таким образом запутать преследователей. Все-таки пара флотанов на хвосте это не шутка.'
        '' + initialstory + '\nИ сейчас необходимо было в кратчайшие сроки добраться до корабля минуя это поле. '
                            '\nВдруг уточка заметил древний бластер на земле. Уточка решил занять позицию и '
                            'отстреливаться от флотанов до последнего '
                            '\nТак как уточка не раз попадад в передряги, то ситуация обернулась в его пользу и со '
                            'временем все флотаны были уничтожены '
    )
    print(nobirdstoryend)


def step_3_go_to_the_bird():
    print(
        '\nПришелец-утка подошел к птице. Птица посмотрела на него осмысленным взглядом.'
        '\nРазмером она было где-то 3 метров в высоту и примерно 5 метров в длинну.'
        '\nВдруг пришелец-утка понял, что необходимо забраться на спину птице, чтобы она'
        '\nперенесла его к кораблю. Так он и поступил. Из-за кустов показались флотана с бластерами,' \
        '\nно птица уже взмыла в воздух и стремительно набирала высоту.'
        '\nФлотаны выстрелили пару раз по птице и уточке, но промахнулись.'
        '\nПтица отнесла пришельца-утку прямо к кораблю.'
    )
    print(birdstoryend)


def step_3_run_on():
    print(
        '\n Пришелец-уточка решил не рисковать и продолжил двигаться по лесу в сторону своего корабля.'
        '\n Вдруг он услышал крики флотанов недалеко от себя. Уточка решил спрятаться и переждать в пещере,'
        '\n на которую он только что случайно наткунулся. Прошло несколько часов. Судя по данным со сканера '
        '\n на корабле, жизненных форм на планете не осталось. Хорошо что уточка установил систему активной '
        '\n маскировки и флотаны не смогли обнаружить его корабль и улетели ни с чем. '
    )
    print(nobirdstoryend)


if __name__ == '__main__':
    initial_step()
